import time
import sys
t=int(input("\n 前？："))
list=[]                 #初始化数据
scale = 20
start = time.perf_counter()
jd=0
print("")



def panduan(i):         #验证质数（大于等于2）
    if  i%2==0:
         return None
    else:
         for j in range(2, int(0.5*i-0.5)):
             if (i % j == 0):
                 return None
                 break
         else:
             return i


def shuaxin(i):         #刷新进度条
    c = 100*i/t
    i=zhuanhuan(i)
    a = "#" * i
    b = " " * (scale - i)
    dur = time.perf_counter() - start
    print("\r {:^5.1f}% [{}{}] {:^.2f}s".format(c,a,b,dur),end = "")
  
def zhuanhuan(i):
    ii=int(scale*i/t)
    return ii

if t == 1:               #1、2特殊情况直接输出
    shuaxin(1)
    print("\n\n 在前1个正整数中，共有0个质数，占比0.00%，运算耗时0.00s。")
    sys.exit(0)
elif t == 2:
    shuaxin(2)
    print("\n\n 在前2个正整数中，共有1个质数，占比50.00%，运算耗时0.00s。")
    sys.exit(0)

for i in range(1,t+1):    #主循环
    if i == t:
        shuaxin(i)
        print("\n\n","在前{}个正整数中，共有{}个质数，占比{:^.2f}%，运算耗时{:^.2f}s。\n"
              .format(t,len(list),100*len(list)/t,time.perf_counter() - start))
    elif panduan(i)!=None:
        list.append(i)
        shuaxin(i)
    else:
        shuaxin(i)
