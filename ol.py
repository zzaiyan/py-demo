import time
start = time.perf_counter()
#print(2,end=',')
for n in range(3,200000,2):
    if n > 10 and n % 10 == 5:
        continue
    else:
        for a in range(2,int(n**0.5)+1):
            if n % a == 0:
                break
usetime = (time.perf_counter() - start)
print(usetime)
